import os
import discord
from dotenv import load_dotenv
import tensorflow as tf
import pickle
from google.cloud import bigquery

# setting discord envirmomonet #
load_dotenv()
token = os.getenv('DISCORD_TOKEN')
conversation = False
client = discord.Client()
WHITELIST = '0123456789abcdefghijklmnopqrstuvwxyz '

with open('tokenizer.pickle', 'rb') as handle:
    tokenizertest = pickle.load(handle)
with open('tagtokenizer.pickle', 'rb') as handle:
    tagtokenizer = pickle.load(handle)
model = tf.keras.models.Sequential()
model.add(tf.keras.layers.Dense(1024, input_shape=(6000,), activation='relu'))
model.add(tf.keras.layers.Dense(512, activation='relu'))
model.add(tf.keras.layers.Dense(len(tagtokenizer.classes_), activation='sigmoid'))
model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])

def Nmaxelements(list1, N): 
    final_list = [] 
    percantage = []
    for i in range(0, N):  
        max1 = 0
        for j in range(len(list1)):      
            if list1[j] > max1: 
                max1 = list1[j];
                test = j    
        list1.remove(max1);
        if(max1 >= 0.4):
            final_list.append(test)
            percantage.append(max1) 
    return final_list, percantage

def getLinks(predictedTags):
    tagsentence = ""
    for tag in predictedTags:
        tagsentence += tag + "|"
    resultLinks = []
    tagsentence = tagsentence[:-1] 
    client = bigquery.Client()
    job_config = bigquery.QueryJobConfig(
        query_parameters=[
            bigquery.ScalarQueryParameter("tagsentence", "STRING", tagsentence),
        ])

    query_job = client.query("""
        SELECT
          ques.id
        FROM `bigquery-public-data.stackoverflow.posts_questions` as ques,  `bigquery-public-data.stackoverflow.posts_answers` as ans
        WHERE ques.tags like '%javascript%' AND ques.tags like @tagsentence AND ques.accepted_answer_id is NOT NULL AND ques.accepted_answer_id = ans.id AND ques.tags is NOT NULL AND ques.id is NOT NULL
        ORDER BY ques.view_count DESC
        LIMIT 3""", job_config= job_config)

    results = query_job.result()
    for row in results:
        resultLinks.append("https://stackoverflow.com/questions/"+ str(row.id))
    return resultLinks

def filterSentences(sentence, whitelist):
    filteredSentence = ""
    for char in sentence:
        if char.lower() in whitelist:
            filteredSentence += char.lower()
    return filteredSentence

def responseGenerator(question):
    ques =  filterSentences(question,whitelist=WHITELIST)
    ques2num = tokenizertest.texts_to_matrix([ques])
    output = model.predict(ques2num)
    prediction = output[0].tolist()
    tro, percantage = Nmaxelements(prediction, 3)
    print("These are the percantages:", percantage)
    stringTags = []
    for i in tro:
        stringTags.append(tagtokenizer.classes_[i])
    result = []
    if(len(stringTags) > 1):
        result = getLinks(stringTags)
        if len(result) == 0 and len(stringTags) == 3:
            print("in repeat")
            result = getLinks(stringTags[:-1])
        linkSentence = ""
        for link in result:
            linkSentence += link + " , "
        finishedSentence = ''
    if len(result) == 0:
        finishedSentence = "I don't know the answer"
    else:
        finishedSentence = "I hope these links help you : " + linkSentence
    return finishedSentence
# discord upon connecting #
@client.event
async def on_ready():
    model.load_weights("training_1/tag.ckpt")
    print(f'{client.user} has connected to Discord!')

# when user send message to discord bot event #
@client.event
async def on_message(message):
    question = ''
    global conversation
    if message.author == client.user:
        return
    if "!help" not in message.content.lower() and conversation == False:
        await message.author.send('Hello '+ message.author.name + ' to ask question to StackGenie write: !help')
        return
    elif conversation == True:
        question = message.content
        response = responseGenerator(question)
        await message.author.send(response)
        conversation = False
    elif "!help" in message.content.lower():
        question = message.content.replace('!help','')
        if question == '':
            await message.author.send(os.getenv('GREETING'))
            #START OF CONVERSATION
            conversation = True
        else:
            await message.author.send(os.getenv('GREETINGWITHQUESTION'))
            #START AND END OF CONVERSATION
            response = responseGenerator(question)
            await message.author.send(response)
            conversation = False
client.run(token)




