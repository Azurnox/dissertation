from google.cloud import bigquery
import nltk
import itertools
from collections import defaultdict
import tensorflow as tf
import matplotlib.pyplot as plt
import matplotlib.ticker as ticker
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import MultiLabelBinarizer
from sklearn.preprocessing import LabelEncoder
import datetime
import unicodedata
import re
import os
import io
import time
import numpy as np
import pickle

WHITELIST = '0123456789abcdefghijklmnopqrstuvwxyz '

limit = {
    'maxq': 40,
    'minq': 0
}
VOCAB_SIZE = 6000
questions = []
tags = []


def getDataFromApi():
    client = bigquery.Client()
    query_job = client.query("""
        SELECT
          ques.id,
          ques.title,
          ques.tags
        FROM `bigquery-public-data.stackoverflow.posts_questions` as ques,  `bigquery-public-data.stackoverflow.posts_answers` as ans
        WHERE ques.tags like '%javascript%' AND ques.accepted_answer_id is NOT NULL AND ques.accepted_answer_id = ans.id AND ques.tags is NOT NULL AND ques.id is NOT NULL
        ORDER BY ques.view_count DESC
        LIMIT 60000  """)

    results = query_job.result()
    global questions, tags
    for row in results:
        questions.append(row.title)
        tags.append(row.tags)

def filterSentences(sentences, whitelist):
    filterSentenceList = []
    for sentence in sentences:
        filteredSentence = ""
        for char in sentence:
            if char.lower() in whitelist:
                filteredSentence += char.lower()
        filterSentenceList.append(filteredSentence)
    return filterSentenceList

def createTagList(tags):
    tagList = []
    filteredTags = []
    for tag in tags:
        differentTags = tag.split("|")
        filteredTags.append(differentTags)
        for singleTag in differentTags:
            if(singleTag not in tagList):
                tagList.append(singleTag)
    return tagList, filteredTags

def layerFilter(sentences, tags):
    layeredList = []
    tagslayerList = []
    for index in range(len(sentences)):
        words = sentences[index].split(" ")
        if len(words) >= limit['minq'] and len(words) <= limit['maxq']:
            layeredList.append(sentences[index])
            tagslayerList.append(tags[index])
    return layeredList, tagslayerList

def tokenize(tokenized_sentences):
    tokenizer = tf.keras.preprocessing.text.Tokenizer(filters='', num_words = VOCAB_SIZE)
    tokenizer.fit_on_texts(tokenized_sentences)
    matrix = tokenizer.texts_to_matrix(tokenized_sentences)
    return matrix, tokenizer

def tagsToNumber(tags, tagList, divider):
    encoder = MultiLabelBinarizer()
    tags_encoded = encoder.fit_transform(tags)
    divider = int(len(cleanQuestion) * 0.8)
    train_tags = tags_encoded[:divider]
    test_tags = tags_encoded[divider:]
    return train_tags, test_tags,encoder

def Nmaxelements(list1, N): 
    final_list = [] 
    percantage = []
    for i in range(0, N):  
        max1 = 0
        for j in range(len(list1)):      
            if list1[j] > max1: 
                max1 = list1[j];
                test = j    
        list1.remove(max1);
        if(max1 >= 0.5):
            final_list.append(test)
            percantage.append(max1) 
    return final_list, percantage
        
def getLinks(predictedTags):
    tagsentence = ""
    for tag in predictedTags:
        tagsentence += tag + "|"
    resultLinks = []
    tagsentence = tagsentence[:-1] 
    client = bigquery.Client()
    job_config = bigquery.QueryJobConfig(
        query_parameters=[
            bigquery.ScalarQueryParameter("tagsentence", "STRING", tagsentence),
        ])

    query_job = client.query("""
        SELECT
          ques.id
        FROM `bigquery-public-data.stackoverflow.posts_questions` as ques,  `bigquery-public-data.stackoverflow.posts_answers` as ans
        WHERE ques.tags like '%javascript%' AND ques.tags like @tagsentence AND ques.accepted_answer_id is NOT NULL AND ques.accepted_answer_id = ans.id AND ques.tags is NOT NULL AND ques.id is NOT NULL
        ORDER BY ques.view_count DESC
        LIMIT 3""", job_config= job_config)

    results = query_job.result()
    for row in results:
        resultLinks.append("https://stackoverflow.com/questions/"+ str(row.id))
    return resultLinks

if __name__ == '__main__':
    ### Preprocessing ###
    getDataFromApi()
    filteredQuestion = filterSentences(questions, whitelist= WHITELIST)
    tagList, filteredTags = createTagList(tags)
    cleanQuestion, cleanTags = layerFilter(filteredQuestion, filteredTags)
    divider = int(len(cleanQuestion) * 0.8)
    train_question = cleanQuestion[:divider]
    test_question = cleanQuestion[divider:]
    train_tensors, train_tokenizer = tokenize(train_question)
    test_tenors = train_tokenizer.texts_to_matrix(test_question)
    train_tags, test_tags, encoder = tagsToNumber(cleanTags, tagList, divider)

    with open('tokenizer.pickle', 'wb') as handle:
        pickle.dump(train_tokenizer, handle, protocol=pickle.HIGHEST_PROTOCOL)

    with open('tagtokenizer.pickle', 'wb') as handle:
        pickle.dump(encoder, handle, protocol=pickle.HIGHEST_PROTOCOL)

    checkpoint_path = "training_1/tag.ckpt"
    checkpoint_dir = os.path.dirname(checkpoint_path)
    cp_callback = tf.keras.callbacks.ModelCheckpoint(filepath=checkpoint_path,
                                                     save_weights_only= True,
                                                     verbose=1)
    ### tag model ###
    model = tf.keras.models.Sequential()
    model.add(tf.keras.layers.Dense(1024, input_shape=(VOCAB_SIZE,), activation='relu'))
    model.add(tf.keras.layers.Dense(512, activation='relu'))
    model.add(tf.keras.layers.Dense(len(tagList), activation='sigmoid'))
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['accuracy'])
    log_dir = "logs\\fit\\" + datetime.datetime.now().strftime("%Y%m%d-%H%M%S")
    tensorboard_callback = tf.keras.callbacks.TensorBoard(log_dir=log_dir, histogram_freq=1)
    model.fit(train_tensors, train_tags, epochs= 30, batch_size = 128, validation_split = 0.1, callbacks=[cp_callback, tensorboard_callback])
    
   
    ### tag model evalulate ###
    print(len(tagList))
    # uncommment once the model is trained
    # model.load_weights(checkpoint_path)
    model.evaluate(test_tenors, test_tags, batch_size= 128)
    test = ["substring in javascript"]
    with open('tokenizer.pickle', 'rb') as handle:
        tokenizertest = pickle.load(handle)
    testNum = tokenizertest.texts_to_matrix(test)
    output = model.predict(testNum)
    prediction = output[0].tolist()
    tro, pre = Nmaxelements(prediction, 3)
    stringTags = []
    for i in tro:
        stringTags.append(encoder.classes_[i])
    result = getLinks(stringTags)
    if len(result) == 0:
        result = getLinks(stringTags[:-1])
    print(result)