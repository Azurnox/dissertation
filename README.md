# Dissertation

This project is doing to be dedicated to a dissertation called 'Measuring the effectiveness of AI-enabled chatbots in the context of online self-guided learning'.

# Project structure
    # Papers
        Contains the reached papers used in the dissertation
        # Notes
            Contains all the papers reacher in point format
        # Research Papers
            Contains all the highlighted points inside the papers
    # SOI
        Contains the disseration proposal
    # Dissertation Document 
        # Contains the final document in pdf formats
    # Prototype
        # Contains all the code implemented for the prototype
